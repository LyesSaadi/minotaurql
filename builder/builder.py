"""Classes and function for all databases."""

from abc import ABC, abstractmethod
import re


class AbstractBuilder(ABC):
    """Base Builder object."""

    def __init__(self, db, model=None):
        """Initialise the class."""
        self.__db = db
        self.__model = model

    @staticmethod
    def __unsupported(action=None):
        """Define unsupported actions."""

        def f(self):
            raise UnsupportedError(action)
        return f

    create_table = __unsupported("Create table")

    delete_table = __unsupported("Delete table")

    rename_table = __unsupported("Rename table")

    create_column = __unsupported("Create column")

    delete_column = __unsupported("Delete column")

    modify_column = __unsupported("Modify column")

    rename_column = __unsupported("Rename column")

    add_entry = __unsupported("Add entry")

    remove_entry = __unsupported("Remove entry")

    def format(self, query, *args, **kwargs):
        """
        Format a provided query.

        Example:
        'DROP TABLE {name}', {"name": "test"}
        => 'DROP TABLE "test"'
        """
        # Looking into macros
        regex_macros = re.compile(
            r"{% ?(?:(db|model|self)\.)?((?:\w+\.)*\w+) ?%}")
        ignore = 0
        while macro := regex_macros.search(query, ignore):
            target = self.__model
            if macro[1] and macro[1] != "model":
                if macro[1] == "db":
                    target = self.__db
                elif macro[1] == "self":
                    target = self
                else:
                    raise Exception("Congratulation! "
                                    + "You have reached an unreachable state!")

            var = macro[2].split(".")
            target_reached = True
            for v in var:
                if hasattr(target, v):
                    target = getattr(target, v)
                else:
                    target_reached = False
                    break

            if target_reached and hasattr(target, "__str__"):
                query = query.replace(macro[0], str(target))
            else:
                raise Exception('Bad macro "' + macro[0] + '" at position '
                                + str(macro.start()))

        # Looking into args
        formated_args = list()
        for var in args:
            if isinstance(var, str):
                formated_args.append('"' + var.replace('"', '""') + '"')
            elif isinstance(var, (int, float)):
                formated_args.append(var)
            else:
                formated_args.append('"' + str(var).replace('"', '""') + '"')
        args = formated_args

        # Looking into kwargs
        for i, var in kwargs.items():
            if isinstance(var, str):
                kwargs[i] = '"' + var.replace('"', '""') + '"'
            elif isinstance(var, (int, float)):
                kwargs[i] = var
            else:
                kwargs[i] = '"' + str(var).replace('"', '""') + '"'

        return query.format(*args, **kwargs)

    @staticmethod
    def autoformat(function):
        """Format automatically the query using a decorator."""
        def f(self, *f_args, **f_kwargs):
            query, args, kwargs = function(self, *f_args, **f_kwargs)

            return self.format(query, *args, **kwargs)
        return f

    @staticmethod
    def autoformatlist(function):
        """Format automatically the queries using a decorator."""
        def f(self, *f_args, **f_kwargs):
            query_list = function(self, *f_args, **f_kwargs)
            result = list()

            for query in query_list:
                result.append(self.format(query[0], *query[1], **query[2]))

            return result
        return f

    @staticmethod
    def autoquery(query):
        """
        Automatically create a query function from a string.

        THIS IS NOT A DECORATOR, even though its structure resemble one.
        """
        def f(self, *args, **kwargs):
            return self.format(query, *args, **kwargs)
        return f


class UnsupportedError(Exception):
    """Exception when a certain builder does not support a certain action."""

    def __init__(self, action=None):
        """Initialise the Error."""
        if action:
            self.message = ("The builder doesn't support the action \""
                            + action + "\".")
        else:
            self.message = "The builder doesn't support this action."

"""SQLite Base Builder."""

from builder.sql.sql import SQLBuilder as b


class SQLiteBuilder(b):
    """Query builder for SQLite DBs."""

    def __init__(self, db, model=None):
        """Initialise the class."""
        b.__init__(self, db, model)

    delete_column = b.__unsupported("Delete column")

    rename_column = b.autoquery(
        "ALTER TABLE {% name %} RENAME {column} TO {name}")

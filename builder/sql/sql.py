"""SQL Base Builder."""

from builder.builder import AbstractBuilder as b


class SQLBuilder(b):
    """Query builder for SQL-type DBs."""

    def __init__(self, db, model=None):
        """Initialise the class."""
        b.__init__(self, db, model)

    @b.autoformat
    def create_table(self, table, **columns):
        """Query."""
        if not columns:
            return "CREATE TABLE {} (id INTEGER PRIMARY KEY)", [table]
        else:
            query = "CREATE TABLE {} (\n"
            args = [table]
            if "id" not in columns:
                query += "id INTEGER PRIMARY KEY,\n"
            for name, properties in columns.items():
                query += "{} {}"
                args += [name, properties["type"]]
                if ("nullable" in properties
                        and properties["nullable"] is False):
                    query += " NOT NULL"
                if ("unique" in properties
                        and properties["unique"] is True):
                    query += " UNIQUE"
                if "default" in properties:
                    query += " DEFAULT {}"
                    args.append(properties["default"])
                query += ",\n"
            query = query[:-2] + "\n)"
            return query, args

    delete_table = b.autoquery("DROP TABLE {% name %}")

    rename_table = b.autoquery("ALTER TABLE {% name %} RENAME TO {name}")

    @b.autoformat
    def create_column(self, table, column, _type, **properties):
        """Query."""
        if not properties:
            return "ALTER TABLE {} ADD {} {}", [table, column, _type]
        else:
            query = "ALTER TABLE {} ADD {} {}"
            args = [table, column, _type]
            if ("nullable" in properties
                    and properties["nullable"] is False):
                query += " NOT NULL"
            if ("unique" in properties
                    and properties["unique"] is True):
                query += " UNIQUE"
            if "default" in properties:
                query += " DEFAULT {}"
                args.append(properties["default"])
            return query, args

    delete_column = b.autoquery("ALTER TABLE {% name %} DROP COLUMN {column}")

    @b.autoformat
    def add_entry(self, table, **values):
        """Query."""
        if not values:
            # Is this compatible with all SQL DBs though?
            return "INSERT INTO {} DEFAULT VALUES", [table]
        query = "INSERT INTO {} "
        query += "(" + ", ".join(["{}"] * len(values.keys())) + ") VALUES "
        args = [table] + list(values.keys())
        if isinstance(values[list(values.keys())[0]], (tuple, list)):
            length = len(list(values.values())[0])
            if length == 0:
                raise Exception("Nothing to insert.")
            for val in values.values():
                if length != len(val):
                    raise Exception("Inconsistent number of values.")
            query += ", ".join([
                               "("
                               + ", ".join(["{}"] * len(values))
                               + ")"
                               ] * len(values[list(values.keys())[0]])
                               )
            for i in range(length):
                for j in values.keys():
                    args.append(values[j][i])
        if isinstance(values[list(values.keys())[0]], dict):
            keys = list(values[list(values.keys())[0]].keys())
            if len(keys) == 0:
                raise Exception("No data provided.")
            for val in values.values():
                for k in val.keys():
                    if k not in keys:
                        raise Exception("Inconsistent number of values.")
            temp = {}
            for column in values.values():
                for key, value in column.items():
                    if key not in temp:
                        temp[key] = list()
                    temp[key].append(value)
            for key in keys:
                query += "(" + ", ".join(["{}"] * len(temp[key])) + "), "
                args += list(temp[key])
            query = query[:-2]
        else:
            query += "(" + ", ".join(["{}"] * len(values.keys())) + ")"
            args += values.values()
        return query, args

    remove_entry = b.autoquery("DELETE FROM {% name %} WHERE id={id}")

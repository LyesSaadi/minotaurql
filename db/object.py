"""Objects controlling databases."""

import re


class DB:
    """The main database object."""

    socket = None

    def __init__(self, url, **args):
        """Create the DB by initiating the connection."""
        self.args = args
        self.url = url

    def connect(self):
        """Initiate connection to the DB socket."""
        pass

    def exec(self, *requests):
        """Execute requests to the DB."""
        pass

    def _get_url(self):
        return self._url

    supported_databases = r"(sqlite|mysql|mariadb)"

    def _set_url(self, url):
        _type = re.compile(r"^(\w*):\/\/.+").match(url).group(1)
        complete_regex = re.compile(r"^" + self.supported_databases +
                                    r"\:\/\/(.*)\/(.*)$")
        regex_without_db = re.compile(r"^" + self.supported_databases +
                                      r"\:\/\/(.*)$")
        self._url = url

        if _type == "sqlite":
            pass
            # return sqliteDB(url, **args)
        else:
            raise Exception("Unsupported database.")

        if m := complete_regex.match(url):
            self.db_type = m.group(1)
            self.path = m.group(2)
            self.working_db = m.group(3)
        if m := regex_without_db.match(url):
            self.db_type = m.group(1)
            self.path = m.group(2)
            self.working_db = None

        if ("auto_reconnect_on_url_change" in self.args and
                self.args["auto_connect_on_url_change"]):
            self.connect()

    url = property(_get_url, _set_url)


"""
This should disappear:
    # Here all functions have a shortcut one to execute the query returned.
    # While the original one is only used for internal purpose to be able to
    # execute multiple things in the same time, it is best to use the shortcut
    # (without an underscore "_" preceding it).

    #
    # Internal functions:
    #

    # Inputs

    def _create_table(self, table, **columns):
        \"\"\"Return a query to create a DB table.\"\"\"
        pass

    def _delete_table(self, table):
        \"\"\"Return a query to delete a DB table.\"\"\"
        pass

    def _rename_table(self, table, new_name):
        \"\"\"Return a query to rename a DB table.\"\"\"
        pass

    def _create_column(self, table, column, _type, **properties):
        \"\"\"Return a query to create a DB column.\"\"\"
        pass

    def _delete_column(self, table, column):
        \"\"\"Return a query to delete a DB column.\"\"\"
        pass

    def _modify_column(self, table, column, **properties):
        \"\"\"Return a query to modify a DB column.\"\"\"
        pass

    def _rename_column(self, table, column, new_name):
        \"\"\"Return a query to rename a DB column.\"\"\"
        pass

    def _add_entry(self, table, **values):
        \"\"\"Return a query to add an entry to a DB.\"\"\"
        pass

    def _remove_entry(self, table, _id):
        \"\"\"Return a query to remove an entry from a DB.\"\"\"
        pass

    #
    # Shortcut function:
    #

    def __getattr__(self, name):
        \"\"\"Shortcut function to avoid copy pasting.\"\"\"
        if hasattr(self, "_" + name):
            def execution(*args, **kwargs):
                self.exec(getattr(self, "_" + name)(*args, **kwargs))
            return execution
        else:
            raise AttributeError(f"'{self.__class__.__name__}' has no "
                                 + f"attribute '{name}'")
"""

"""Functions and objects for query conditions."""

from abc import ABC, abstractmethod
import enum


class Condition(ABC):
    """Object representing query conditions."""

    def __init__(self, o1, op, o2=None):
        """Create a condition object."""
        self.o1 = o1
        self.op = op
        self.o2 = o2
        if op == Operator.EQ and o2 is None and o1 is not None:
            self.op = Operator.NU
        elif op == Operator.NE and o2 is None and o1 is not None:
            self.op = Operator.NN

    def __or__(self, other):
        """OR operation."""
        return Condition(self, Operator.OR, other)

    def __ror__(self, other):
        """OR operation."""
        return self.__or__(other)

    def __ior__(self, other):
        """OR operation."""
        return self.__or__(other)

    def _or(self, other):
        """OR operation."""
        return self.__or__(other)

    def __and__(self, other):
        """AND operation."""
        return Condition(self, Operator.AND, other)

    def __rand__(self, other):
        """AND operation."""
        return self.__and__(other)

    def __iand__(self, other):
        """AND operation."""
        return self.__and__(other)

    def _and(self, other):
        """AND operation."""
        return self.__and__(other)

    def __invert__(self):
        """NOT operation."""
        return Condition(self, Operator.NOT, None)

    def __pos__(self):
        """Do nothing, only here for consistency."""
        return self

    def __neg__(self):
        """NOT operation."""
        return self.__invert__()

    def _not(self):
        """NOT operation."""
        return self.__invert__()

    def _in(self, array):
        """IN operation."""
        return Condition(self, Operator.IN, array)

    def _between(self, _min, _max):
        """BETWEEN operation."""
        return Condition(self, Operator.BE, (_min, _max))

    def _like(self, pattern):
        """LIKE operation."""
        return Condition(self, Operator.BE, pattern)

    def _is(self, val):
        """IS operation."""
        if val is True:
            return Condition(self, Operator.TR)
        elif val is False:
            return Condition(self, Operator.FA)
        elif val is None:
            return Condition(self, Operator.NU)
        else:
            return Condition(self, Operator.EQ, val)

    def _is_not(self, val):
        """IS NOT operation."""
        if val is True:
            return Condition(self, Operator.FA)
        elif val is False:
            return Condition(self, Operator.TR)
        elif val is None:
            return Condition(self, Operator.NN)
        else:
            return Condition(self, Operator.NE, val)

    @abstractmethod
    def __repr__(self):
        """Transform the conditions to a string."""
        pass

    def __str__(self):
        """Transform the conditions to a string."""
        return self.__repr__()

    def __get_string(self, _object):
        """Get the name of a column or the object stringified."""
        if _object is None:
            return ""
        elif isinstance(_object, Condition):
            return "(" + repr(_object) + ")"
        elif isinstance(_object, bool):
            if _object is True:
                return "'t'"
            elif _object is False:
                return "'f'"
        else:
            return str(_object)


class Operator(enum.Enum):
    """List of operators."""

    # Comparison
    EQ = enum.auto()   # Equal => ==
    NE = enum.auto()   # Different => != / <>
    GT = enum.auto()   # Greater Than => >
    LT = enum.auto()   # Lower Than => <
    GE = enum.auto()   # Greater or Equal => >=
    LE = enum.auto()   # Lower or Equal => <=
    TR = enum.auto()   # Is True
    FA = enum.auto()   # Is False
    NU = enum.auto()   # Is NULL
    NN = enum.auto()   # Is Not NULL

    # Logical
    IN = enum.auto()   # Value inside the array
    BE = enum.auto()   # Value between two values
    LI = enum.auto()   # Value similar to a string (LIKE)

    # TODO: Add support for subqueries.
    # ALL = enum.auto()
    # EXI = enum.auto()
    # ANY = enum.auto()

    OR = enum.auto()   # OR logical gate
    AND = enum.auto()  # AND logical gate
    NOT = enum.auto()  # NOT logical gate

# Welcome to minotaurql !

minotaurql (or MINimalistic Orm To All yoUR Query Languages) is a project
created in order to alleviate the burden of interacting with databases on Python
while being compatible with the most Query Languages possible.

## Principles

minautorql is based of the principles of OOP and exploit python mecanisms to
create a transparent management of databases.

### Example
```python
from minautorql.model import Model

class User(Model):
    first_name = str
    last_name = str
    age = int
```
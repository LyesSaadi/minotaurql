"""Basic functions used through the project."""

import re

# Official DBs for auto
from db.sqlite import sqliteDB


def auto(url, **args):
    """
    Return the appropriate DB object from the provided URL.

    It guesses the DB type from the provided URL.
    """
    _type = re.compile(r"^(\w*):\/\/.+").match(url).group(1)
    if _type == "sqlite":
        return sqliteDB(url, **args)
    else:
        raise Exception("Unsupported database.")
